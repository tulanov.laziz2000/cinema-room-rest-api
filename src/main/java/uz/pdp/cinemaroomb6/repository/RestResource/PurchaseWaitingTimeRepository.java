package uz.pdp.cinemaroomb6.repository.RestResource;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.cinemaroomb6.model.PurchaseWaitingTime;

import java.util.UUID;

@RepositoryRestResource(collectionResourceRel = "purchase-waiting-time",path = "purchase-waiting-time")
public interface PurchaseWaitingTimeRepository extends JpaRepository<PurchaseWaitingTime, UUID> {
}
