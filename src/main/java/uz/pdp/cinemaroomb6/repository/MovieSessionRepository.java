package uz.pdp.cinemaroomb6.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.pdp.cinemaroomb6.model.MovieSession;
import uz.pdp.cinemaroomb6.projection.MovieSessionByIdProjection;
import uz.pdp.cinemaroomb6.projection.MovieSessionProjection;
import uz.pdp.cinemaroomb6.projection.MovieSessionQRDataProjection;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Repository
public interface MovieSessionRepository extends JpaRepository<MovieSession, UUID> {


    @Query(nativeQuery = true,
    value = "select cast(ms.id as varchar) as sessionId," +
            "cast(ms.session_date_id as varchar) as sessionDateId," +
            "sd.date as sessionDate," +
            "cast(ms.start_time_id as varchar) as sessionStartTimeId," +
            "cast(ms.end_time_id as varchar) as sessionEndTimeId," +
            "(select st.time from session_times st where st.id = ms.start_time_id)\n" +
            "               as sessionStartTime,\n" +
            "       (select st.time from session_times st where st.id = ms.end_time_id)\n" +
            "               as sessionEndTime," +
            "cast(ms.movie_announcement_id as varchar) as sessionMovieAnnouncementId " +
            "from movie_sessions ms " +
            "join session_dates sd on ms.session_date_id = sd.id " +
            "where ms.hall_id=:hallId")
    List<MovieSessionProjection> findByHallId(UUID hallId);

    @Query(nativeQuery = true,
            value = "select cast(ms.id as varchar) as id " +
                    " from movie_sessions ms " +
                    "where ms.id=:sessionId")
    MovieSessionByIdProjection findBySessionId(UUID sessionId);


    @Query(nativeQuery = true,
    value = "select h.name as sessionHallName," +
            "sd.date as sessionDate," +
            "(select st.time from session_times st where st.id = ms.start_time_id)" +
            " as sessionStartTime," +
            "(select st.time from session_times st where st.id = ms.end_time_id)" +
            "as sessionEndTime " +
            "from movie_sessions ms " +
            "join halls h on h.id = ms.hall_id " +
            "join session_dates sd on ms.session_date_id = sd.id " +
            "where ms.id=:sessionId")
    MovieSessionQRDataProjection findBySessionIdQRData(UUID sessionId);


    @Query(nativeQuery = true,
    value = "select sd.date from movie_sessions ms " +
            "join session_dates sd on ms.session_date_id = sd.id " +
            "where ms.id=:movieSessionId")
    LocalDate getDateByTicketMovieSessionId(MovieSession movieSessionId);
}
