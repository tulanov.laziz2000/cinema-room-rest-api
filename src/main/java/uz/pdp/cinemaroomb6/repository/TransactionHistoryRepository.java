package uz.pdp.cinemaroomb6.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.pdp.cinemaroomb6.model.TransactionHistory;

import java.util.UUID;

@Repository
public interface TransactionHistoryRepository extends JpaRepository<TransactionHistory, UUID> {

    @Query(nativeQuery = true,
            value = "select th.stripe_payment_intent  from transaction_history th " +
                    "join transaction_ticket tt on th.id = tt.transaction_history_id " +
                    "where tt.ticket_id=:ticketId")
    String getPaymentIntent(UUID ticketId);

}
