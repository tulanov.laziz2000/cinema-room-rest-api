package uz.pdp.cinemaroomb6.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.cinemaroomb6.service.StripeServiceImpl;
import uz.pdp.cinemaroomb6.service.TicketServiceImpl;

@RestController
public class StripePaymentController {

    @Autowired
    TicketServiceImpl ticketService;

    @Autowired
    StripeServiceImpl stripeService;

    @RequestMapping(value = "transaction-succeeded", method = RequestMethod.GET)
    public HttpEntity<?> getSuccessMessage() {
        return ResponseEntity.ok("Payment succeeded");
    }

    @RequestMapping(value = "transaction-failed", method = RequestMethod.GET)
    public HttpEntity<?> getFailMessage() {
        return ResponseEntity.ok("Payment failed");
    }


    // Using the Spark framework (http://sparkjava.com)
    @RequestMapping(value = "stripe-webhook", method = RequestMethod.POST)
    public HttpEntity<?> handle(@RequestBody String payload, @RequestHeader("Stripe-Signature")String sigHeader) {
        return stripeService.handle(payload,sigHeader);

    }
}
