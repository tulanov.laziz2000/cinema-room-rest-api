package uz.pdp.cinemaroomb6.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.cinemaroomb6.service.StripeServiceImpl;
import uz.pdp.cinemaroomb6.service.TicketServiceImpl;

import java.util.UUID;

@RestController
@RequestMapping("/api/ticket")
public class TicketController {

    @Autowired
    TicketServiceImpl ticketService;

    @Autowired
    StripeServiceImpl stripeService;

    @PostMapping("/add-to-cart")
    public HttpEntity<?> addTicketToCart(@RequestParam("session-id")UUID sessionId,
                                   @RequestParam("seat-id")UUID seatId){
        return ticketService.addTicketToCart(seatId,sessionId);

    }

    @GetMapping("/purchase")
    public HttpEntity<?> purchaseTicket(){
        return stripeService.createStripeSession();
    }

    @GetMapping("/view-my-cart")
    public HttpEntity<?> getCurrentUserTickets(){
        return ticketService.getCurrentUserTickets();
    }
    @PostMapping("/refund")
    public HttpEntity<?> refundTicket(@RequestParam("ticket-id")UUID ticketId) {
        return ticketService.refundTicket(ticketId);
    }
//    @PostMapping("/refund-new")
//    public HttpEntity<?> refundTicketNew(@RequestBody List<TicketDto>ticketDtoList){
//        return ticketService.refundTicketNew(ticketDtoList);
//    }
}
