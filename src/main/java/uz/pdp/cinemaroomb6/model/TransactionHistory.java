package uz.pdp.cinemaroomb6.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.cinemaroomb6.model.template.AbsEntity;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "transaction_history")
public class TransactionHistory extends AbsEntity {


    @ManyToMany
    @JoinTable(
            name = "transaction_ticket",
            joinColumns = @JoinColumn(name = "transaction_history_id"),
            inverseJoinColumns = @JoinColumn(name = "ticket_id")
    )
    private List<Ticket> ticketList;

    @ManyToOne
    private PayType payType;

    private Double amount;

    private boolean isRefunded;

    private String stripePaymentIntent;
}
