package uz.pdp.cinemaroomb6.model.enums;

public enum TicketStatus {
    NEW,
    PURCHASED,
    REFUNDED
}
