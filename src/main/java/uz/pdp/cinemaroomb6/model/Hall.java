package uz.pdp.cinemaroomb6.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.cinemaroomb6.model.template.AbsEntity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;


@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "halls")
public class Hall extends AbsEntity {

    @Column(nullable = false,length = 50)
    private String name;

    private Double vipAddFeeInPercent;

    @OneToMany(mappedBy = "hall", cascade = CascadeType.ALL)
    private List<Row> rows;

    public Hall(String name) {
        this.name = name;
    }

    public Hall(String name, Double vipAddFeeInPercent) {
        this.name = name;
        this.vipAddFeeInPercent = vipAddFeeInPercent;
    }

    @Override
    public String toString() {
        return "Hall{" +
                "name='" + name + '\'' +
                ", vipAddFeeInPercent=" + vipAddFeeInPercent +
                "} " + super.toString();
    }
}
