package uz.pdp.cinemaroomb6.service;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.IElement;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import org.springframework.stereotype.Service;
import uz.pdp.cinemaroomb6.model.Ticket;
import uz.pdp.cinemaroomb6.projection.PdfTicketProjection;
import uz.pdp.cinemaroomb6.service.interfaces.DocService;

import java.io.*;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

@Service
public class DocServiceImpl implements DocService {


    public void createQRCode(String data) throws WriterException, IOException {

//data that we want to store in the QR code
//        String data= "THE HABIT OF PERSISTENCE IS THE HABIT OF VICTORY.";
//path where we want to get QR Code
        String path = "C:\\OFFLINE Bootcamp Java\\Rest API Projects\\Cinema ROOM Rest Service\\Cinema-room-b6\\src\\main\\resources\\static\\QRCode.png";
//Encoding charset to be used
        String charset = "UTF-8";
        Map<EncodeHintType, ErrorCorrectionLevel> hashMap = new HashMap<>();
//generates QR code with Low level(L) error correction capability
        hashMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
//invoking the user-defined method that creates the QR code
        generateQRcode(data, path, charset, hashMap, 200, 200);//increase or decrease height and width accodingly
//prints if the QR code is generated
        System.out.println("QR Code created successfully.");
    }

//    @Override
//    public  String createQRCodeItext(String data, String qrCodeName) throws DocumentException, FileNotFoundException {
//        //Step - 1 :Create Document object that will hold the code
//        Document qr_code_Example = new Document(new Rectangle(360, 852));
//        // Step-2: Create PdfWriter object for the document
//        String path="C:\\OFFLINE Bootcamp Java\\Rest API Projects\\Cinema ROOM Rest Service\\Cinema-room-b6\\src\\main\\resources\\static\\" + qrCodeName + ".pdf";
//        PdfWriter writer = PdfWriter.getInstance(qr_code_Example, new FileOutputStream(path));
//        // Step -3: Open document for editing
//        qr_code_Example.open();
//        //Step-4: Create New paragraph for QR Summary
////        qr_code_Example.add(new Paragraph("Wow, we created a QR Code in PDF document using iText Java"));
//        //Step-5: Create QR Code by using BarcodeQRCode Class
//        BarcodeQRCode my_code = new BarcodeQRCode(data, 200, 400, null);
//        //Step-6: Get Image corresponding to the input string
//        Image qr_image = my_code.getImage();
//        //Step-7: Stamp the QR image into the PDF document
//        qr_code_Example.add(qr_image);
//        //Step-8: Close the PDF document
//        qr_code_Example.close();
//        return path;
//    }



    private void generateQRcode(String data, String path, String charset, Map map, int h, int w) throws WriterException, IOException {
//the BitMatrix class represents the 2D matrix of bits
//MultiFormatWriter is a factory class that finds the appropriate Writer subclass for the BarcodeFormat requested and encodes the barcode with the supplied contents.
        BitMatrix matrix = new MultiFormatWriter().encode(new String(data.getBytes(charset), charset), BarcodeFormat.QR_CODE, w, h);
        MatrixToImageWriter.writeToPath(matrix, path.substring(path.lastIndexOf('.') + 1), new File(path).toPath());
    }


    public void generatePdf(PdfTicketProjection projection) throws MalformedURLException, FileNotFoundException {
        String pdfPath = "C:\\OFFLINE Bootcamp Java\\Rest API Projects\\Cinema ROOM Rest Service\\Cinema-room-b6\\src\\main\\resources\\TestPdf.pdf";
        com.itextpdf.kernel.pdf.PdfWriter pdfWriter=new com.itextpdf.kernel.pdf.PdfWriter(pdfPath);
        PdfDocument pdfDocument = new PdfDocument(pdfWriter);
        pdfDocument.addNewPage();
        com.itextpdf.layout.Document document = new com.itextpdf.layout.Document(pdfDocument);

        ImageData data= ImageDataFactory.create("C:\\OFFLINE Bootcamp Java\\Rest API Projects\\Cinema ROOM Rest Service\\Cinema-room-b6\\src\\main\\resources\\static\\QRCode.png");

        com.itextpdf.layout.element.Image image=new com.itextpdf.layout.element.Image(data);
        image.setWidth(100);
        image.setHeight(100);

        float[] bigColumnWidth = {250F, 250F};
        Table bigtable = new Table(bigColumnWidth);

        float[] smallColumnWidth = {50F,50F};
        Table smallTable = new Table(smallColumnWidth);


        Paragraph movie = new Paragraph("Movie:");
        Paragraph hall= new Paragraph("Hall:");
        Paragraph row = new Paragraph("Row:");
        Paragraph seat = new Paragraph("Seat:");
        Paragraph date = new Paragraph("Date:");

        smallTable.addCell(new Cell().add(movie));
        smallTable.addCell(new Cell().add(new Paragraph(projection.getMovieTitle())));
        smallTable.addCell(new Cell().add(hall));
        smallTable.addCell(new Cell().add(new Paragraph(projection.getHallName())));
        smallTable.addCell(new Cell().add(row));
        smallTable.addCell(new Cell().add(new Paragraph(projection.getRowNumber().toString())));
        smallTable.addCell(new Cell().add(seat));
        smallTable.addCell(new Cell().add(new Paragraph(projection.getSeatnumber().toString())));
        smallTable.addCell(new Cell().add(date));
        smallTable.addCell(new Cell().add(new Paragraph(projection.getSessionDate().toString())));
        smallTable.setFontSize(18);


        bigtable.addCell(new Cell().add(smallTable));
        bigtable.addCell(new Cell().add(image));


        for (IElement child : smallTable.getChildren()) {
            ((Cell) child).setBorder(Border.NO_BORDER);
        }
        for (IElement child : bigtable.getChildren()) {
            ((Cell) child).setBorder(Border.NO_BORDER);
        }
        document.add(bigtable);
        document.close();
    }
    public byte[] getByteArrayFromFile() throws IOException {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final InputStream in = new FileInputStream("C:\\OFFLINE Bootcamp Java\\Rest API Projects\\Cinema ROOM Rest Service\\Cinema-room-b6\\src\\main\\resources\\static\\TestPdf.pdf");
        final byte[] buffer = new byte[500];

        int read;
        while ((read = in.read(buffer)) > 0) {
            baos.write(buffer, 0, read);
        }
        in.close();

        return baos.toByteArray();
    }

}
