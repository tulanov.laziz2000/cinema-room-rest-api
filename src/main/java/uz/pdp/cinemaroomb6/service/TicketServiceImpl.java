package uz.pdp.cinemaroomb6.service;

import com.stripe.model.checkout.Session;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.cinemaroomb6.exceptions.NotFoundException;
import uz.pdp.cinemaroomb6.model.*;
import uz.pdp.cinemaroomb6.model.enums.TicketStatus;
import uz.pdp.cinemaroomb6.payload.ApiResponse;
import uz.pdp.cinemaroomb6.projection.MovieSessionQRDataProjection;
import uz.pdp.cinemaroomb6.projection.PdfTicketProjection;
import uz.pdp.cinemaroomb6.projection.TicketProjection;
import uz.pdp.cinemaroomb6.repository.*;
import uz.pdp.cinemaroomb6.service.interfaces.TicketService;

import javax.mail.MessagingException;
import java.time.LocalDate;
import java.time.Period;
import java.util.*;

import static uz.pdp.cinemaroomb6.utils.Constants.USER_ID;


@Service
public class TicketServiceImpl implements TicketService {

    @Value("${STRIPE-SECRET-KEY}")
    String stripeApiKey;


    @Autowired
    TicketRepository ticketRepository;

    @Autowired
    MovieSessionRepository movieSessionRepository;

    @Autowired
    SeatRepository seatRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    TransactionHistoryRepository transactionHistoryRepository;

    @Autowired
    StripeServiceImpl stripeService;

    @Autowired
    SendMessageServiceImpl emailSendService;

    @Autowired
    DocServiceImpl docService;

    @Override
    public HttpEntity<?> addTicketToCart(UUID seatId, UUID sessionId) {
        try {
            MovieSession movieSession = movieSessionRepository.getById(sessionId);
            Seat seat = seatRepository.getById(seatId);
            MovieSessionQRDataProjection movieSessionQRData = movieSessionRepository.findBySessionIdQRData(sessionId);
            Double price = ticketRepository.getTicketPrice(seatId, sessionId);
            Ticket ticket = new Ticket(
                    movieSession,
                    seat,
                    "" + movieSessionQRData,
                    price,
                    userRepository.getById(UUID.fromString(USER_ID)),
                    TicketStatus.NEW
            );
            Ticket savedTicket = ticketRepository.save(ticket);
            scheduleDeleteTicket(savedTicket);
            return ResponseEntity.ok(
                    new ApiResponse("Success", true)
            );
        } catch (Exception e) {
            return ResponseEntity.ok(
                    new ApiResponse("Failed", false)
            );
        }
    }

    @Override
    public HttpEntity<?> purchaseTicket(UUID userId, Session session) {
        try {
            List<Ticket> ticketsByUserId = ticketRepository.findAllByUserIdAndStatus(userId,TicketStatus.NEW);
            double ticketPrice = 0;
            for (Ticket ticket : ticketsByUserId) {
//                if (ticket.getStatus().equals(TicketStatus.NEW)) {
                    ticketPrice+=ticketRepository.getTicketPrice(ticket.getSeat().getId(),ticket.getMovieSession().getId());
                    ticket.setStatus(TicketStatus.PURCHASED);
                    ticketRepository.save(ticket);
//                }
            }


//            PdfTicketProjection projection=ticketRepository.getPdfTicket(ticketsByUserId.get(0).getId());
//            docService.createQRCode("Bu qrcode");
//            docService.generatePdf(projection);
//            Thread thread = new Thread(() -> {
//                try {
//                    emailSendService.sendMessageWithAttachment(
//                            "tulanov.laziz2000@gmail.com",
//                            "Ticket",
//                            "You got your ticket",
//                            "C:\\OFFLINE Bootcamp Java\\Rest API Projects\\Cinema ROOM Rest Service\\Cinema-room-b6\\src\\main\\resources\\TestPdf.pdf"
//                    );
//                } catch (MessagingException e) {
//                    e.printStackTrace();
//                }
//            });
//            thread.start();
            transactionHistoryRepository.save(
                    new TransactionHistory(
                            ticketsByUserId, null, ticketPrice, false, session.getPaymentIntent())
            );
            return ResponseEntity.ok(
                    new ApiResponse("success", true)
            );
        } catch (Exception e) {
            return ResponseEntity.ok(
                    new ApiResponse("failed", false)
            );
        }
    }



    @Override
    public HttpEntity<?> getCurrentUserTickets() {
        List<TicketProjection> userTickets = ticketRepository.getTicketsByUserId(
                UUID.fromString(USER_ID)
        );
        return ResponseEntity.ok(new ApiResponse("success", true, userTickets));
    }

    @SneakyThrows
    @Override
    public HttpEntity<?> refundTicket(UUID ticketId) {
        Ticket ticket = ticketRepository.findById(ticketId).orElseThrow(() ->
                new NotFoundException("Ticket: " + ticketId + " not found")
        );
        if (ticket.getStatus().equals(TicketStatus.PURCHASED)) {
            LocalDate sessionDate =
                    movieSessionRepository.getDateByTicketMovieSessionId(ticket.getMovieSession());
            Period period = Period.between(LocalDate.now(), sessionDate);
            double refundSum;
            if (period.getDays() <= 1) {
                refundSum = ticket.getPrice() * 0.2*100;
            } else {
                refundSum = ticket.getPrice() * 0.5*100;
            }
            String paymentIntent = transactionHistoryRepository.getPaymentIntent(ticketId);
            if (stripeService.refundTicket(paymentIntent, refundSum )) {
                ticket.setStatus(TicketStatus.REFUNDED);
                ticketRepository.save(ticket);
                transactionHistoryRepository.save(
                        new TransactionHistory(Collections.singletonList(ticket),null,refundSum/100,
                                true,null
                                )
                );
                return ResponseEntity.ok(
                        new ApiResponse("success", true)
                );
            } else {
                return ResponseEntity.ok(
                        new ApiResponse("failed", false)
                );
            }
        } else {
            return ResponseEntity.ok(
                    new ApiResponse("failed", false)
            );
        }
    }



    @Override
    public void scheduleDeleteTicket(Ticket savedTicket) {
        TimerTask ticketDeleteTask = new TimerTask() {
            @Override
            public void run() {
                Ticket ticketFromDB = ticketRepository.findById(savedTicket.getId()).orElseThrow(
                        () -> new ResourceNotFoundException("Ticket: " + savedTicket.getId() + " not found")
                );
                if (ticketFromDB.getStatus().equals(TicketStatus.NEW)) {
                    ticketRepository.delete(ticketFromDB);
                }
            }
        };
        Timer timer = new Timer();
        timer.schedule(ticketDeleteTask, 300000);
    }

}
