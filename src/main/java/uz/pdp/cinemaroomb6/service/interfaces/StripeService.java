package uz.pdp.cinemaroomb6.service.interfaces;

import com.stripe.model.checkout.Session;
import org.springframework.http.HttpEntity;

public interface StripeService {

    HttpEntity<?> handle(String payload, String sigHeader);

    HttpEntity<?> completeTicketPurchase(Session session);

    HttpEntity<?> createStripeSession();

    boolean refundTicket(String paymentIntent,Double refundSum);

}
