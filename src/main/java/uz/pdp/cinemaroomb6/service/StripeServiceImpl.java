package uz.pdp.cinemaroomb6.service;


import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.Event;
import com.stripe.model.Refund;
import com.stripe.model.checkout.Session;
import com.stripe.net.Webhook;
import com.stripe.param.RefundCreateParams;
import com.stripe.param.checkout.SessionCreateParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.cinemaroomb6.payload.ApiResponse;
import uz.pdp.cinemaroomb6.projection.TicketProjection;
import uz.pdp.cinemaroomb6.repository.TicketRepository;
import uz.pdp.cinemaroomb6.service.interfaces.StripeService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static uz.pdp.cinemaroomb6.utils.Constants.*;

@Service
public class StripeServiceImpl implements StripeService {


    @Value("${STRIPE-SECRET-KEY}")
    String stripeApiKey;

    @Value("${ENDPOINTSECRET}")
    String endpointSecret;

    @Autowired
    TicketServiceImpl ticketService;

    @Autowired
    TicketRepository ticketRepository;

    @Override
    public HttpEntity<?> handle(String payload, String sigHeader) {

        Stripe.apiKey = stripeApiKey;
        System.out.println("Got payload: " + payload);
        Event event = null;
        try {
            event = Webhook.constructEvent(payload, sigHeader, endpointSecret);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Handle the checkout.session.completed event
        if (event.getType().equals("checkout.session.completed")) {
            Session session = (Session) event.getDataObjectDeserializer().getObject().get();
            // Fulfill the purchase...
//            System.out.println(session.getPaymentIntent());
            return completeTicketPurchase(session);
        } else {
            return ResponseEntity.ok(
                    new ApiResponse("failed", false)
            );
        }
    }

    @Override
    public HttpEntity<?> completeTicketPurchase(Session session) {
        String userId = session.getClientReferenceId();
        return ticketService.purchaseTicket(UUID.fromString(userId),session);
//        System.out.println("Fulfilling order..."+session.getClientReferenceId());
    }

    @Override
    public HttpEntity<?> createStripeSession() {
        try {
            Stripe.apiKey = stripeApiKey;
            List<TicketProjection> userTickets = ticketRepository.getTicketsByUserId(
                    UUID.fromString(USER_ID)
            );
            ArrayList<SessionCreateParams.LineItem> lineItems = new ArrayList<>();
            for (TicketProjection userTicket : userTickets) {
                SessionCreateParams.LineItem.PriceData.ProductData productData = SessionCreateParams.LineItem.PriceData.ProductData
                        .builder()
                        .setName(userTicket.getMovieTitle())
                        .build();


                Double price=(userTicket.getPrice() + FIXED_STRIPE_FEE)/(1-PERCENT_STRIPE_FEE)*100;
                System.out.println(price);
                SessionCreateParams.LineItem.PriceData priceData = SessionCreateParams.LineItem.PriceData
                        .builder()
                        .setCurrency("usd")
                        .setUnitAmount(price.longValue())
                        .setProductData(productData)
                        .build();


                SessionCreateParams.LineItem lineItem = SessionCreateParams.LineItem
                        .builder()
                        .setPriceData(priceData)
                        .setQuantity(1L)
                        .build();
                lineItems.add(lineItem);

            }
            SessionCreateParams params = SessionCreateParams
                    .builder()
                    .addAllLineItem(lineItems)
                    .setCancelUrl("http://localhost:8080/transaction-fail")
                    .setSuccessUrl("http://localhost:8080/transaction-succeeded")
                    .setMode(SessionCreateParams.Mode.PAYMENT)
                    .setClientReferenceId(USER_ID)
                    .build();

            Session session = Session.create(params);
            String url = session.getUrl();
            return ResponseEntity.ok(
                    new ApiResponse("success", true, url)
            );
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.ok(
                    new ApiResponse("failed", false)
            );
        }
    }

    @Override
    public boolean refundTicket(String paymentIntent,Double refundSum) {
        try {
            Stripe.apiKey = stripeApiKey;
            RefundCreateParams params = RefundCreateParams
                    .builder()
                    .setPaymentIntent(paymentIntent)
                    .setAmount(refundSum.longValue())
                    .build();
            Refund refund=Refund.create(params);
            return refund.getStatus().equals("succeeded");
        } catch (StripeException e) {
            e.printStackTrace();
            return false;
        }
    }
}
