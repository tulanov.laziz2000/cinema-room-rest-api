package uz.pdp.cinemaroomb6.projection;

import org.springframework.beans.factory.annotation.Value;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.UUID;

public interface MovieSessionQRDataProjection {

    String getSessionHallName();
    LocalDate getSessionDate();
    LocalTime getSessionStartTime();
    LocalTime getSessionEndTime();
}
