package uz.pdp.cinemaroomb6.projection;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.UUID;

public interface MovieSessionProjection {

    UUID getSessionId();
    UUID getSessionMovieAnnouncementId();
    UUID getSessionDateId();
    LocalDate getSessionDate();
    UUID getSessionStartTimeId();
    LocalTime getSessionStartTime();
    UUID getSessionEndTimeId();
    LocalTime getSessionEndTime();
}
